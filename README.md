# vue-cli-plugin-fweb-template

> Fracweb Template for vue-cli

Before you try to create a new project, you must be connected to Fractal's VPN. Once you are connected, run:

```bash
npm install -g @vue/cli

vue create -p bitbucket:fractalindustries/vue-cli-plugin-fweb-template -r https://artifactory.fractalindustries.com/artifactory/api/npm/fractal-npm-virtual/ my-project
```

## Configuration

This plugin has a number of customizable settings. The first prompt lets you choose between a single and multipage deployment.

Single page deployments looks like s standard `vue create <project-name>` with some additional nice to have settings and standardizations. The multipage app setting allows us to access controller our apps. We want the ability to completely fence off functionality unless the user has the appropiate permissions.

```javacript
{
  "useConfigFiles": true,
  "plugins": {
    "@vue/cli-plugin-babel": {},
    "@vue/cli-plugin-eslint": {
      "config": "prettier",
      "lintOn": ["save", "commit"]
    },
    "@vue/cli-plugin-unit-jest": {}
  },
  "router": true,
  "routerHistoryMode": true,
  "vuex": true,
  "cssPreprocessor": "sass"
}
```

## TODO

- hook up fracweb-vue-components
- implement brand color selection
- add fractal fontawesome repos
- add direction for resolving plugin on bitbucket
