# Architecture

- [`.vscode`](#vscode)
- [`docs`](#docs)
- [`public`](#public)
  - [`index.html`](#indexhtml)
- [`src`](#src)
  - [`api`](#api)
  - [`assets`](#assets)
  - [`shared-components`](#shared-components)
  - [`design`](#design)
  - [`pages`](#pages)
    - [`index`](#index)
      - [`router`](#router)
      - [`app.vue`](#appvue)
      - [`main.js`](#mainjs)
  - [`shared-layouts`](#shared-layouts)
  - [`share-views`](#share-views)
  - [`state`](#state)
  - [`utils`](#utils)
  - [`vue.config.js`](#vueconfigjs)
  - [`vue.config.pages.js`](#vueconfigpagesjs)
  - [`vue.webpack.aliases.config.js`](#vuewebpackaliasesconfigjs)
  - [`vue.webpack.config.js`](#vuewebpackconfigjs)
- [`tests`](#tests)

## `.vscode`

Settings and extensions specific to this project, for Visual Studio Code. See [the editors doc](editors.md#visual-studio-code) for more.

## `docs`

You found me! :wink:

## `public`

Where you'll keep any static assets, to be added to the `dist`(This could change) directory without processing from our build system.

### `index.html`

This one file actually _does_ get processed by our build system, allowing us to inject some information from Webpack with [EJS](http://ejs.co/), such as the title, then add our JS and CSS.

## `src`

Where we keep all our source files.

### `api`

Where we keep our api files. Learn more about [the api doc](api.md).

### `assets`

This project manages assets via Vue CLI. Learn more about [its asset handling here](https://github.com/vuejs/vue-cli/blob/dev/docs/assets.md).

### `shared-components`

Where most of the components that can be used across pages in our app will live, including our [global base components](development.md#base-components).

### `design`

Where we keep our [design variables and tooling](tech.md#design-variables-and-tooling).

### `pages`

Where page specific code live. See [the pages doc](pages.md) for more.

#### `index`

Where the router, routes, and any routing-related components live. See [the pages doc](pages.md#index) for more.

##### `router`

Where the router, routes, and any routing-related components live that are specific to that page live. See [the routing doc](routing.md) for more.

##### `app.vue`

The root Vue component that simply delegates to the router view. This is typically the only component to contain global CSS.

##### `main.js`

The entry point to our app, were we create our Vue instance and mount it to the DOM.

### `state`

Where all our global state management lives. See [the state management doc](state.md) for more.

### `utils`

These are utility functions you may want to share between many files in your application. They will always be pure and never have side effects, meaning if you provide a function the same arguments, it will always return the same result. These should also never directly affect the DOM or interface with our Vuex state. [the utils doc](utils.md)

### `vue.config.js`

### `vue.config.pages.js`

### `vue.webpack.aliases.config.js`

### `vue.webpack.config.js`

## `tests`

Where all our tests go. See [the tests doc](tests.md) for more.
