import example from './example'

describe('@utils/example', () => {
  it('correctly returns string', () => {
    let stringTest = 'test'
    let functionReturn = example(stringTest)
    expect(functionReturn).toEqual('test')
  })
})
