const path = require('path')

function resolveSrc(_path) {
  return path.join(__dirname, _path)
}

const aliases = {
  '@': '.',
  '@src': 'src',
  '@components': 'src/components',
  '@assets': 'src/assets',
  '@utils': 'src/utils',
  '@design': 'src/design/index.scss',
  '@globals': 'src/globals.scss',
  <%_ if (options.config === 'single') { _%>
  '@layouts': 'src/router/layouts',
  '@views': 'src/router/views/',
  '@router': 'src/router/',
  '@state': 'src/state',
  <%_ } else if (options.config === 'multi') { _%>
  '@pages': 'src/pages',
  '@shared-layouts': 'src/shared-layouts',
  '@shared-components': 'src/shared-components',
  '@shared-views': 'src/shared-views/',
  <%_ } _%>
}

module.exports = {
  webpack: {},
  jest: {}
}

for (const alias in aliases) {
  const aliasTo = aliases[alias];
  module.exports.webpack[alias] = resolveSrc(aliasTo);
  module.exports.jest["^" + alias + "/(.*)$"] = "<rootDir>/" + aliasTo + "/$1";
}
