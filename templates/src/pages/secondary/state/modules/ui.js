const types = {
  TOGGLE_SIDEBAR: 'TOGGLE_SIDEBAR',
  TOGGLE_MENU_CHILDREN: 'TOGGLE_MENU_CHILDREN'
}

export const state = {
  sidebarCollapsed: false,
  menu: [
    {
      id: 1,
      name: 'Using this Library',
      path: '/',
      expanded: false,
      external: false,
      iconPath: require('@assets/svg/placeholder.svg'),
      children: []
    },
    {
      id: 2,
      name: 'Route 1',
      path: '/route1',
      expanded: false,
      external: false,
      iconPath: require('@assets/svg/placeholder.svg'),
      children: []
    },
    {
      id: 3,
      name: 'Route 2',
      path: '/route2',
      expanded: false,
      external: false,
      iconPath: require('@assets/svg/placeholder.svg'),
      children: [{ name: 'Child 1', path: '/child1', id: 1 }]
    }
  ]
}

export const getters = {
  sidebarCollapsed: state => state.sidebarCollapsed,
  menu: state => state.menu
}

export const actions = {
  // eslint-disable-next-line
  toggleSidebar({ commit, state }) {
    commit(types.TOGGLE_SIDEBAR)
  },
  toggleMenuChildren({ commit }, payload) {
    commit(types.TOGGLE_MENU_CHILDREN, payload)
  }
}

export const mutations = {
  [types.TOGGLE_SIDEBAR](state) {
    state.sidebarCollapsed = !state.sidebarCollapsed
  },
  [types.TOGGLE_MENU_CHILDREN](state, { id, expanded }) {
    state.menu.find(item => item.id === id).expanded = !expanded
  }
}
