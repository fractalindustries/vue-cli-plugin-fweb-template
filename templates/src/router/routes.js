// eslint-disable-next-line
import store from '@state/store'
import lazyLoadView from '@utils/lazy-load-view.js'

export default [
  {
    path: '/',
    name: 'home',
    component: () => lazyLoadView(import('@views/Home'))
  },
  {
    path: '/route1',
    name: 'route1',
    component: () => lazyLoadView(import('@views/Route1'))
  },
  {
    path: '/route2',
    name: 'route2',
    component: require('@views/route2/index').default,
    children: [
      {
        name: '/',
        path: 'child1',
        component: () => lazyLoadView(import('@views/route2/Child1'))
      }
    ]
  },
  {
    path: '/404',
    name: '404',
    component: require('@views/404').default,
    // Allows props to be passed to the 404 page through route
    // params, such as `resource` to define what wasn't found.
    props: true
  },
  // Redirect any unmatched routes to the 404 page. This may
  // require some server configuration to work in production:
  // https://router.vuejs.org/en/essentials/history-mode.html#example-server-configurations
  {
    path: '*',
    component: require('@views/404').default
  }
]
