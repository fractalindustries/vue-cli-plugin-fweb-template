# Building and deploying to production

- [From the terminal](#from-the-terminal)

## From the terminal

```bash
# Build for production with minification
npm run build
```
