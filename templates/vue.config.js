var webpackConfig = require('./vue.webpack.config.js')

module.exports = {
  // Merged into the final Webpack config
  configureWebpack: webpackConfig,
  pages: require('./vue.config.pages.js').pages,
  outputDir: '../webapp/'
  // devServer: {
  //   //webpack server
  //   port: 8081,
  //   //open browser on start
  //   open: true,
  //   //lift server for local REST API calls
  //   proxy: 'http://localhost:8080'
  // }
}
