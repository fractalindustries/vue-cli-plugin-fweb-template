const fs = require('fs')

function removeFileOrDir(path) {
  if (!fs.lstatSync(path)) return

  const stat = fs.lstatSync(path)

  if (stat.isDirectory()) {
    deleteFolderRecursive(path)
    return
  }

  fs.unlink(path, err => {
    if (err) throw err
  })

  function deleteFolderRecursive(path) {
    if (fs.existsSync(path)) {
      fs.readdirSync(path).forEach(function(file, index) {
        var curPath = path + '/' + file
        if (fs.lstatSync(curPath).isDirectory()) {
          // recurse
          deleteFolderRecursive(curPath)
        } else {
          // delete file
          fs.unlinkSync(curPath)
        }
      })
      fs.rmdirSync(path)
    }
  }
}

module.exports = (api, { config }, rootOptions) => {
  let pkg = {
    scripts: {
      lintFix:
        'eslint--ext.js,.vue--fix. && stylelint--fix "src/**/*.vue" "src/**/*.scss" && markdownlint docs/*.md *.md && prettier --list-different --write "**/*.{js,json,css,scss,vue,md}"'
    },
    dependencies: {},
    devDependencies: {}
  }

  Object.assign(pkg.dependencies, {
    '@fortawesome/fontawesome-pro-light': '5.0.13',
    '@fortawesome/fontawesome-pro-regular': '5.0.13',
    '@fortawesome/fontawesome-pro-solid': '5.0.13',
    '@fortawesome/fontawesome-pro-brands': '1.1.8',
    '@fortawesome/fontawesome-svg-core': '^1.2.2',
    '@fortawesome/vue-fontawesome': '^0.1.1',
    axios: '^0.18.0',
    lodash: '^4.17.11',
    'normalize.css': '^8.0.0',
    nprogress: '^0.2.0',
    'vue-portal': '^1.0.0',
    'fracweb-vue-components': '^1.0.3',
    vuelidate: '^0.7.4'
  })

  Object.assign(pkg.devDependencies, {
    '@vue/eslint-config-standard': '^3.0.4',
    stylelint: '^9.4.0',
    'stylelint-config-css-modules': '^1.3.0',
    'stylelint-config-prettier': '^3.3.0',
    'stylelint-config-recess-order': '^2.0.0',
    'stylelint-config-standard': '^18.2.0',
    'stylelint-scss': '^3.2.0',
    'jest-transform-stub': '^1.0.0'
  })

  api.extendPackage(pkg)

  // Build out templates
  api.render('./templates')

  api.onCreateComplete(() => {
    const unneededFiles = ['router.js', 'store.js', 'views']

    if (config === 'single') {
      var completeListOfFiles = unneededFiles.concat([
        'shared-layouts',
        'shared-views',
        'shared-components',
        'pages'
      ])
    }

    if (config === 'multi') {
      var completeListOfFiles = unneededFiles.concat([
        'App.vue',
        'main.js',
        'router',
        'components',
        'state'
      ])
    }

    // Removes files that are unused by the app
    fs.readdir(api.resolve('./src'), (err, files) => {
      files.forEach(file => {
        if (completeListOfFiles.includes(file)) {
          removeFileOrDir(`${api.resolve('./src')}/${file}`)
        }
      })
    })

    if (config === 'single') {
      const fs = require('fs')
      const mainPath = api.resolve(`./src/main.js`)

      const newMain = `
        import Vue from "vue";
        import App from "./App.vue";
        import router from "@router";
        import store from "@state/store";

        // Don't warn about using the dev version of Vue in development
        Vue.config.productionTip = process.env.NODE_ENV === "production";

        new Vue({
        router,
        store,
        render: h => h(App)
        }).$mount("#app");
        `

      let contentMain = fs.readFileSync(mainPath, { encoding: 'utf-8' })

      contentMain = newMain
      fs.writeFileSync(mainPath, contentMain, { encoding: 'utf-8' })
    }
  })
}
