# Troubleshooting

These are some troubleshooting tips for more common issues people might run into while developing, including more information on what might be happening and how to fix the problem.

- [Visual Studio (VS) Code formatting issues](#visual-studio-vs-code-formatting-issues)

## Visual Studio (VS) Code formatting issues

If you're using VS Code and notice that some files are being formatted incorrectly on save, the source is probably a formatter extension you've installed. The reason you're seeing it now is that this project enables the `editor.formatOnSave` setting. Previously, that extension was probably just doing nothing. To fix the problem, you'll need to either properly configure the extension or, if it's simply broken, uninstall it.

Extensions with known issues include:

- [Visual Studio Code Format](https://marketplace.visualstudio.com/items?itemName=ryannaddy.vscode-format#review-details)
