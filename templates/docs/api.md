# API

As you build out your api requests, use this to document each api file. The files should be named after the service they are integrating with.

- [`auth.js`](#authjs)

## `auth.js`

This is an example of using the `src/utils/http.js` helper file to assemble an http request
