# Utils

- [`http.js`](#httpjs)
- [`lazyLoadView.js`](#lazyLoadViewjs)

## `http.js`

## `lazyLoadView.js`

Lazy-loads view components, but with better UX. A loading view will be used if the component takes a while to load, falling back to a timeout view in case the page fails to load.
