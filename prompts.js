module.exports = [
  {
    name: 'config',
    type: 'list',
    message: `Pick a file structure:`,
    choices: [
      {
        name: 'Single page, one entry point',
        value: 'single',
        short: 'Single Page'
      },
      {
        name: 'Multipage, adds pages',
        value: 'multi',
        short: 'Multipage'
      }
    ]
  },
  {
    name: 'brand_color',
    type: 'input',
    message: `Enter a hex color for your brand color. Please enter a valid hex code.`,
    default: '#de3535',
    validate(val) {
      var isOk = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(val)
      return new Promise(function(resolve, reject) {
        if (isOk) return resolve(true)
        return reject('Invalid hex color, please try again', false)
      })
    }
  },
  {
    name: 'product_acronym',
    type: 'input',
    message: `Enter your product acronym.  Must be less than 6 letters.`,
    default: 'none',
    validate(val) {
      var isOk = val.length < 7
      return new Promise(function(resolve, reject) {
        if (isOk) return resolve(true)
        return reject('You have added to many letter.', false)
      })
    }
  }
]
