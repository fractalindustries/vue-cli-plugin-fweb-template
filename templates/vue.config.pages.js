module.exports = {
  pages: {
  <%_ if (options.config === 'single') { _%>
    index: {
      // entry for the page
      entry: 'src/main.js',
      filename: 'index.html',
      title: 'Index Page'
    }
  <%_ } else if (options.config === 'multi') { _%>
    index: {
      // entry for the page
      entry: 'src/pages/index/main.js',
      // output as dist/index.html
      filename: 'index.html',
      title: 'Index Page'
    },
    // when using the entry-only string format,
    // template is inferred to be `public/subpage.html`
    // and falls back to `public/index.html` if not found.
    // Output filename is inferred to be `subpage.html`.
    secondary: {
      entry: 'src/pages/secondary/main.js',
      filename: 'secondary.html',
      title: 'Secondary Page'
    }
  <%_ } _%>
  }
}
