import Vue from 'vue'
import VueRouter from 'vue-router'

// Adds a loading bar at the top during page loads.
import NProgress from 'nprogress/nprogress'
// eslint-disable-next-line
import store from '../state/store'
import routes from './routes'

Vue.use(VueRouter)

const router = new VueRouter({
  routes,
  // Use the HTML5 history API (i.e. normal-looking routes)
  // instead of routes with hashes (e.g. example.com/#/about).
  // This may require some server configuration in production:
  // https://router.vuejs.org/en/essentials/history-mode.html#example-server-configurations
  mode: 'history',
  // Simulate native-like scroll behavior when navigating to a new
  // route and using back/forward buttons.
  // eslint-disable-next-line
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

// After navigation is confirmed, but before resolving...
// eslint-disable-next-line
router.beforeResolve((routeTo, routeFrom, next) => {
  // If this isn't an initial page load...
  if (routeFrom.name) {
    // Start the route progress bar.
    NProgress.configure({ showSpinner: false })
    NProgress.start()
  }
  next()
})

// When each route is finished evaluating...
// eslint-disable-next-line
router.afterEach((routeTo, routeFrom) => {
  // Complete the animation of the route progress bar.
  NProgress.done()
})

export default router
