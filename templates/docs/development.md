# Setup and development

- [First-time setup](#first-time-setup)
- [Installation](#installation)
- [Dev server](#dev-server)
  - [Developing with the production API](#developing-with-the-production-api)
- [Generators](#generators)
- [Aliases](#aliases)
- [Globals](#globals)
  - [Base components](#base-components)

## First-time setup

Make sure you have the following installed:

- [Node](https://nodejs.org/en/) (at least the latest LTS)

Then update the following files to suit your application:

- `src/app.config.js` (provides metadata about your app)

## Installation

```bash
# Install dependencies from package.json
npm install
```

## Dev server

> Note: If you're on Linux and see an `ENOSPC` error when running the commands below, you must [increase the number of available file watchers](https://stackoverflow.com/questions/22475849/node-js-error-enospc#answer-32600959).

```bash
# Launch the dev server
npm run serve
```

## Aliases

To simplify referencing local modules and refactoring, you can set aliases to be shared between dev and unit tests in `aliases.config.js`. As a convention, this project uses an `@` prefix to denote aliases.

## Globals

### Base components

[Base components](https://vuejs.org/v2/style-guide/#Base-component-names-strongly-recommended) (a.k.a. presentational, dumb, or pure components) that apply app-specific styling and conventions should all begin with the `_base-` prefix. Since these components are typically used in place of raw HTML element (and thus used as frequently), they're automatically globally registered for convenience. This means you don't have to import and locally register them to use them in templates.
